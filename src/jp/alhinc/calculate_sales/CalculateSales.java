package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String MONEY_OVER = "合計金額が10桁を超えました";
	private static final String FILE_BAD_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String BAD_BRANCHCODE = "の支店コードが不正です";
	private static final String BAD_COMMODITYCODE = "の商品コードが不正です";
	private static final String BAD_FORMAT = "のフォーマットが不正です";
	private static final String FILE_NOT_NUMBER = "売り上げファイル名が連番になっていません";


	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		//エラー処理3-1
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
		}


		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		//商品コードと商品名を保持するMap
		Map<String,String> commodityNames = new HashMap<>();
		//商品コードと売上金額を保持するMap
		Map<String,Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "[0-9]{3}", "支店")) {
			return;
		}

		//商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "^[A-Za-z0-9]+${8}", "商品")) {
			return;
		}



		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		BufferedReader brdata = null;


		for(int i = 0; i < files.length; i++) {
			if(files[i].isFile() && files[i].getName().matches("[0-9]{8}.rcd")) {
				rcdFiles.add(files[i]);
			}
		}

		//エラー処理2-1
		for(int i = 0; i < rcdFiles.size() - 1 ; i++) {

			int formar = Integer.parseInt(files[i].getName().substring(0,8));
			int latter = Integer.parseInt(files[i + 1].getName().substring(0,8));

			if((latter - formar) != 1) {
				System.out.println(FILE_NOT_NUMBER);
				return;
			}
		}


		for(int i = 0; i < rcdFiles.size(); i++) {

			try {
				FileReader frdata = new FileReader(files[i]);
				brdata = new BufferedReader(frdata);

				List<String> rcdFilesdata = new ArrayList<>();
				String lines;
				while((lines = brdata.readLine()) != null) {
					rcdFilesdata.add(lines);
				}

				//エラー処理2-4 + 追加課題エラー処理2-5
				if(rcdFilesdata.size() != 3) {
					System.out.println(files[i].getName() + BAD_FORMAT);
					return;
				}

				//エラー処理2-3
				if(!branchNames.containsKey(rcdFilesdata.get(0))) {
					System.out.println(files[i].getName() + BAD_BRANCHCODE);
					return;
				}

				//追加課題エラー処理2-4
				if(!commodityNames.containsKey(rcdFilesdata.get(1))) {
					System.out.println(files[i].getName() + BAD_COMMODITYCODE);
				}

				//エラー処理3-2
				if(!rcdFilesdata.get(2).matches("[0-9]{1,10}")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				long filesale = Long.parseLong(rcdFilesdata.get(2));
				Long saleAmount = branchSales.get(rcdFilesdata.get(0)) + filesale;
				Long commoditysaleAmount = commoditySales.get(rcdFilesdata.get(1)) + filesale;


				//エラー処理2-2
				if(saleAmount >= 10000000000L) {
					System.out.println(rcdFilesdata.get(0) + MONEY_OVER);
					return;
				}

				//追加課題エラー処理2-2
				if(commoditysaleAmount >= 10000000000L) {
					System.out.println(rcdFilesdata.get(1) + MONEY_OVER);
					return;
				}


				branchSales.put(rcdFilesdata.get(0), saleAmount);
				commoditySales.put(rcdFilesdata.get(1), commoditysaleAmount);


			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;

			}finally {
				if(brdata != null) {
					try {
						brdata.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}



		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;

		}
		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}


	}



	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales, String regex, String key) {
		BufferedReader br = null;

		try{
			File file = new File(path, fileName);

			//エラー処理1-1
			if(!file.exists()) {
				System.out.println(key + FILE_NOT_EXIST);
				return false;
			}


			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)

				String[] items = line.split(",");

				//エラー処理1-2
				if((items.length != 2) ||(!items[0].matches(regex))) {
					System.out.println(key + FILE_BAD_FORMAT);
					return false;
				}


				names.put(items[0], items[1]);
				sales.put(items[0], 0L);

			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {

			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}


	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)

		BufferedWriter outFile = null;

		try {
			File newFile = new File(path, fileName);
			FileWriter outfile = new FileWriter(newFile);
			outFile = new BufferedWriter(outfile);

			for(String key : names.keySet()) {

				outFile.write(key + "," + names.get(key) + "," + sales.get(key));
				outFile.newLine();

			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		}finally {
			if(outFile != null) {
				try {
					outFile.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}


